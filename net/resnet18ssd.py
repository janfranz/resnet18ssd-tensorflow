import tensorflow as tf
import numpy as np

class ResNet18SSD(object):
	# default_params = 
	def __init__(self, images):
		self._images = images	# input images
	
	def forward(self, images):
		filters = [64, 64, 64, 128, 128, 256, 256, 512, 512]
		kernels = [7, 3, 3, 3, 3, 3, 3, 3, 3]
		strides = [2, 1, 1, 2, 1, 2, 1, 2, 1]
		feature_layers = ['block7', 'block9', 'block10', 'block11', 'block12', 'block13']
		endpoints = {}
		num_classes = 21
		anchor_sizes=[(21., 45.),
                      (45., 99.),
                      (99., 153.),
                      (153., 207.),
                      (207., 261.),
                      (261., 315.)]
		anchor_ratios=[[2, .5],
                       [2, .5, 3, 1./3],
                       [2, .5, 3, 1./3],
                       [2, .5, 3, 1./3],
                       [2, .5],
                       [2, .5]],
        #resnet layers
        #conv layer 1
		with tf.variable_scope('conv1'):
			x = self._conv('conv1', images[0], kernels[0], filters[0], strides=strides[0])
			x = self._batch_norm(x)
			x = self._relu('relu', x)
			x = self._max_pool(x)
		endpoints['block1'] = x
		x = self._residual_block(x, 'residual1', filters[1], kernels[1], strides[1])
		endpoints['block2'] = x
		x = self._residual_block(x, 'residual2', filters[2], kernels[2], strides[2])
		endpoints['block3'] = x
		x = self._residual_block_multipart(x, 'residual3', filters[3], kernels[3], strides[3])
		endpoints['block4'] = x
		x = self._residual_block(x, 'residual4', filters[4], kernels[4], strides[4])
		endpoints['block5'] = x
		x = self._residual_block_multipart(x, 'residual5', filters[5], kernels[5], strides[5])
		endpoints['block6'] = x
		x = self._residual_block(x, 'residual6', filters[6], kernels[6], strides[1])
		endpoints['block7'] = x
		# feature_layers.append(x)
		x = self._residual_block_multipart(x, 'residual7', filters[7], kernels[7], strides[7])
		endpoints['block8'] = x
		x = self._residual_block(x, 'residual8', filters[8], kernels[8], strides[8])
		endpoints['block9'] = x
		# feature_layers.append(x)

        #ssd layers
		x = self._conv('ssd1', x, 1, 256, 1)
		x = self._conv('ssd2', x, 3, 512, 2)
		endpoints['block10'] = x
		# feature_layers.append(x)
		x = self._conv('ssd3', x, 1, 128, 1)
		x = self._conv('ssd4', x, 3, 256, 2)
		endpoints['block11'] = x
		# feature_layers.append(x)
		x = self._conv('ssd5', x, 1, 128, 1)
		x = self._conv('ssd6', x, 3, 256, 2)
		endpoints['block12'] = x
		# feature_layers.append(x)
		x = self._conv('ssd7', x, 1, 128, 1)
		x = self._conv('ssd8', x, 3, 128, 2)
		endpoints['block13'] = x
		# feature_layers.append(x)

        # prediction and localizations
		prediction_fn=tf.nn.softmax
		predictions = []
		logits = []
		localizations = []
		for i, layer in enumerate(feature_layers):
				with tf.variable_scope(layer + '_box'):
					c, l = self.ssd_multibox_layer(endpoints[layer],
												num_classes,
												anchor_sizes,
												anchor_ratios,
												-1)
				predictions.append(prediction_fn(c))
				logits.append(c)
				localizations.append(l)
		print('class predictions: %s' % predictions)
		print('localization predictions: %s' % localizations)
		return predictions, localizations, endpoints
    
	def ssd_multibox_layer(self,
						net,
						num_classes,
                    	sizes,
                    	ratios=[1],
                    	normalization=-1,
                    	bn_normalization=False):
		
		num_anchors = len(sizes) + len(ratios)
		num_loc_pred = num_anchors * 4
		loc_pred = self._conv('conv_loc', net, 3, num_loc_pred, 1)
		loc_pred = tf.reshape(loc_pred,
							self.tensor_shape(loc_pred, 4)[:-1]+[num_anchors, 4])

		num_class_pred = num_anchors * num_classes
		class_pred = self._conv('conv_class', net, 3, num_class_pred, 1)
		class_pred = tf.reshape(class_pred,
							self.tensor_shape(class_pred, 4)[:-1]+[num_anchors, num_classes])

		return class_pred, loc_pred

	def tensor_shape(self, x, rank=3):
		"""Returns the dimensions of a tensor.
		Args:
			image: A N-D Tensor of shape.
		Returns:
			A list of dimensions. Dimensions that are statically known are python
			integers,otherwise they are integer scalar tensors.
		"""
		if x.get_shape().is_fully_defined():
			return x.get_shape().as_list()
		else:
			static_shape = x.get_shape().with_rank(rank).as_list()
			dynamic_shape = tf.unstack(tf.shape(x), rank)
			return [s if s is not None else d
					for s, d in zip(static_shape, dynamic_shape)]
    #====================================================================================
	def _residual_block(self, x, name, num_filters, filter_size, strides):
		out_channel = x.get_shape().as_list()[-1]
		with tf.variable_scope(name) as scope:
			shortcut = x
			x = self._conv('conv_1', x, filter_size, num_filters, strides)
			x = self._batch_norm(x, 'batch_norm_1')
			x = self._relu('relu_1', x)
			x = self._conv('conv_2', x, filter_size, num_filters, strides)
			x = self._batch_norm(x, 'batch_norm_2')
			x = x + shortcut
			x = self._relu('relu_2', x)
		return x
	
	def _residual_block_multipart(self, x, name, num_filters, filter_size, strides):
		out_channel = x.get_shape().as_list()[-1]
		with tf.variable_scope(name) as scope:
			shortcut = self._conv('shortcut', x, 1, num_filters, strides)
			shortcut = self._batch_norm(shortcut, 'batch_norm_shortcut')
		with tf.variable_scope(name) as scope:
			x = self._conv('conv_1', x, filter_size, num_filters, strides)
			x = self._batch_norm(x, 'batch_norm_1')
			x = self._relu('relu_1', x)
			x = self._conv('conv_2', x, filter_size, num_filters, 1)
			x = self._batch_norm(x, 'batch_norm_2')
			x = x + shortcut
			x = self._relu('relu_2', x)

		return x
	
	def _conv(self, name, x, filter_size, out_filters, strides):
		# print('HERE: %s' % x)
		in_filters = x.get_shape().as_list()[-1]
		initializer = tf.random_normal_initializer(stddev=np.sqrt(2.0/(filter_size*filter_size*out_filters)))
		with tf.variable_scope(name):
			kernel = tf.get_variable(
				'kernel', [filter_size, filter_size, in_filters, out_filters],
				tf.float32, initializer=initializer)
			conv = tf.nn.conv2d(x, kernel, [1, strides, strides, 1], padding='SAME')
		
		return conv
		
	def _relu(self, name, x, leakiness=0.0):
		# return tf.nn.relu(x, name='relu')
		if leakiness > 0.0:
			name = 'lrelu' if name is None else name
			return tf.maximum(x, x*leakiness, name='lrelu')
		else:
			name = 'relu' if name is None else name
			return tf.nn.relu(x, name='relu')

	def _batch_norm(self, x, name='batch_norm'):
		decay = 0.9
		with tf.variable_scope(name):
			batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2])
			with tf.device('/CPU:0'):
				mu = tf.get_variable('mu', batch_mean.get_shape(), tf.float32,
							initializer=tf.zeros_initializer(), trainable=False)
				sigma = tf.get_variable('sigma', batch_var.get_shape(), tf.float32,
							initializer=tf.ones_initializer(), trainable=False)
				beta = tf.get_variable('beta', batch_mean.get_shape(), tf.float32,
							initializer=tf.zeros_initializer())
				gamma = tf.get_variable('gamma', batch_var.get_shape(), tf.float32,
							initializer=tf.ones_initializer())

			update = 1.0 - decay
			update_mu = mu.assign_sub(update*(mu - batch_mean))
			update_sigma = sigma.assign_sub(update*(sigma - batch_var))
			tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, update_mu)
			tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, update_sigma)

			# mean, var = tf.cond(is_train, lambda: (batch_mean, batch_var),
													# lambda: (mu, sigma))
			mean, var = (mu, sigma)
			batch_norm = tf.nn.batch_normalization(x, mean, var, beta, gamma, 1e-5)
		
		return batch_norm
		
	def _max_pool(self, x):
		return tf.nn.max_pool(x, [1, 3, 3, 1], [1, 2, 2, 1], 'SAME')

    #===============================================================================

with tf.Graph().as_default():
	sess = tf.Session()
	images = [tf.placeholder(tf.float32, [1, 300, 300, 3])]
	print("Build ResNet-18 SSD model")
	network_train = ResNet18SSD(images)
	network_train.forward(images)
	init = tf.global_variables_initializer()
	sess.run(init)
